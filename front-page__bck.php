<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package ramuca
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
      <div class="page-description">
    		<?php
    		while ( have_posts() ) :
    			the_post();

    			get_template_part( 'template-parts/content', 'page' );

    			// If comments are open or we have at least one comment, load up the comment template.
    			if ( comments_open() || get_comments_number() ) :
    				comments_template();
    			endif;

    		endwhile; // End of the loop.
    		?>
      </div>
			<nav class="home-navigation">
					<?php
					$od_contact_slug_demand = get_theme_mod('od_form_slug__demand');
          $od_contact_slug_offer = get_theme_mod('od_form_slug__offer');
					?>
					<a class="home-navigation__item home-navigation__item--need" href="<?= $od_contact_slug_demand ?>" target="_blank">
							<?= __('Necesito apoyo', 'ramuca') ?>
					</a>
					<a class="home-navigation__item home-navigation__item--offer" href="<?= $od_contact_slug_offer ?>" target="_blank">
							<?= __('Ofrezco apoyo', 'ramuca') ?>
					</a>
			</nav>
		</main><!-- #main -->
		<div class="ramuca-map">
			<h3 class="ramuca-map__label">Grupos locales</h3>
			<div class="ramuca-map__map" id="ramuca-map"></div>
		</div>
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
