<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ramuca
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="ramuca-news">
				<h3 class="ramuca-news__label">
						RAMUCA en twitter
				</h3>
        <a class="twitter-timeline" data-chrome="transparent nofooter noheader noborders" data-width="600" data-height="300" href="https://twitter.com/RAMUCA2020?ref_src=twsrc%5Etfw">Tweets by RAMUCA2020</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
    </div>
		<div class="ramuca-footer">
				<div class="site-info">
					<h5>RAMUCA</h5> | Red de Apoyo Mutuo de los barrios Centro y Macarena. Sevilla, 2020.
				</div><!-- .site-info -->
				<div class="login">
						<a href="/accede">Login</a>
				</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
