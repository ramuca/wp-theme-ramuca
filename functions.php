<?php
/**
 * ramuca functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package ramuca
 */

if ( ! function_exists( 'ramuca_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function ramuca_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on ramuca, use a find and replace
		 * to change 'ramuca' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'ramuca', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'ramuca' ),
			'menu-social' => esc_html__( 'Social', 'ramuca' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'ramuca_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'ramuca_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function ramuca_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'ramuca_content_width', 640 );
}
add_action( 'after_setup_theme', 'ramuca_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function ramuca_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'ramuca' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'ramuca' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'ramuca_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function ramuca_scripts()
{

	wp_enqueue_style( 'ramuca-style', get_template_directory_uri() . '/style/style.css' );

	wp_enqueue_script( 'ramuca-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'ramuca-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	$od_contact_slug = get_theme_mod('od_contact_form_slug');
	if ( is_page($od_contact_slug) ){
			wp_enqueue_script( 'ramuca-od-contact', get_template_directory_uri() . '/js/contact.js', [], '1.0', true );
	}

	if ( is_page('recursos-covid') ){
			wp_enqueue_script( 'ramuca-resources-filters', get_template_directory_uri() . '/js/resources-filters.js', [], '1.0', true );
	}

	if ( is_front_page() ){
		  wp_enqueue_style( 'leaflet-style', get_template_directory_uri() . '/js/leaflet/leaflet.css' );
			wp_enqueue_script( 'leaflet', get_template_directory_uri() . '/js/leaflet/leaflet.js', [], '1.0', true );
			wp_enqueue_script( 'ramuca-map', get_template_directory_uri() . '/js/map.js', ['jquery', 'leaflet'], '1.0', true );
	}
	if ( is_singular('manifest') ){
			wp_enqueue_script( 'ramuca-manifest', get_template_directory_uri() . '/js/manifest.js', [], '1.0', true );
	}
}
add_action( 'wp_enqueue_scripts', 'ramuca_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}


/**
 *  Add custom classes to body
 */

 add_filter('body_class', 'ramuca_custom_classes');

 function ramuca_custom_classes( $classes ) {
      if( is_front_page() ){
          $classes[] = 'no-title';
      }
			if( is_page('sobre-ramuca') ){
          $classes[] = 'about';
      }
      return $classes;
 }

/**
 *   Customise login
 */

 add_action( 'login_enqueue_scripts', 'ramuca_login_css' );

 function ramuca_login_css() {
     wp_enqueue_style( 'ramuca-login', get_stylesheet_directory_uri() . '/style/login.css' );
 }

 add_filter( 'login_headerurl', 'ramuca_login_logo_url' );

 function ramuca_login_logo_url() {
     return home_url();
 }

 //Remove Google ReCaptcha code/badge everywhere apart from select pages
 add_action('wp_print_scripts', function () {
    //Add pages you want to allow to array
    $od_form_demand_slug = get_theme_mod('od_form_slug__demand');
    $od_form_offer_slug = get_theme_mod('od_form_slug__offer');
    if(!is_page([ $od_form_demand_slug, $od_form_offer_slug, 'contacta' ])){
        wp_dequeue_script( 'google-recaptcha' );
        wp_dequeue_script( 'google-invisible-recaptcha' );
    }
 });


// Function to change email address

function ramuca_sender_email( $original_email_address ) {
  return 'no-reply@ramuca.net';
}

// Function to change sender name

function ramuca_sender_name( $original_email_from ) {
  return 'Ramuca';
}

// Hooking up our functions to WordPress filters
add_filter( 'wp_mail_from', 'ramuca_sender_email' );
add_filter( 'wp_mail_from_name', 'ramuca_sender_name' );
