/**
 *  Add map to home
 */
document.addEventListener('DOMContentLoaded', function()
{
    var map = L.map('ramuca-map', {
        scrollWheelZoom: false,
    }).setView([ 37.3808, -5.9700 ], 12);

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);

    var icon = L.icon({
        iconUrl      : '/wp-content/themes/ramuca/style/icons/marker.svg',
        shadowUrl    : '/wp-content/themes/ramuca/style/icons/marker-shadow.svg',
        iconSize     : [ 40, 48 ],
        shadowAnchor : [ 20, 0 ],
    });

    jQuery.get('/wp-json/ramuca-zones/v1/all', function(result){
        result.forEach( function(i){
            L.marker([ i.pos.lat, i.pos.lon ], {
                icon : icon
            }).bindPopup([
                "<div class='ramukita'>",
                    "<h4 class='ramukita__name'>",
                        i.name,
                    "</h4>",
                    "<div class='ramukita__contact'>",
                        "<a href='/contacta?ramukita=" + encodeURIComponent(i.name) +  "' target='_blank'>",
                            "Contacta con este grupo",
                        "</a>",
                        "<p class='ramukita__contact-notes'>",
                        "Si vas a ofrecer o pedir apoyo usa la botonera superior. ",
                        "Usa formulario sólo para cuestiones generales. ",
                        "</p>",
                    "</div>",
                "</div>"
            ].join(""))
            .addTo(map);
        })
    })


})
