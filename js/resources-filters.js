document.addEventListener('DOMContentLoaded', function()
{
    var filters = document.querySelectorAll('.resource-filters__select');
    var filtered = {
        'cat'  : 'all',
        'hood' : 'all'
    };
    var resources = document.querySelectorAll('.resource');
    var empty = document.querySelector('.resources__empty');
    filters.forEach( function(filter){
        filter.addEventListener('change', function(e){
            filtered[e.target.dataset.type] = e.target.value;
            resources.forEach( function(resource){
                var show = true;
                if( filtered.cat != 'all' && filtered.cat != resource.dataset.cat)
                    show = false;
                if( filtered.hood != 'all' && filtered.hood != resource.dataset.hood)
                    show = false;
                if(show){
                    resource.classList.remove('hidden');
                } else {
                    resource.classList.add('hidden');
                }
            });
            if( resources.length == document.querySelectorAll('.resource.hidden').length ){
                empty.classList.remove('hidden');
            } else {
                empty.classList.add('hidden');
            };
        });
    })


});
