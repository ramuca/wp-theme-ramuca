document.addEventListener('DOMContentLoaded', function()
{
    var urlParams = new URLSearchParams(window.location.search);
    var ramukita = urlParams.get('ramukita');
    if(ramukita){
        var topic = document.querySelector('[name=your-subject]');
        topic.value = 'Tengo una consulta sobre/para el grupo ' + ramukita,
        topic.setAttribute('readonly', '')
    }
})
