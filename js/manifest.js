/**
 *  Manifiest scripts
 */

document.addEventListener("DOMContentLoaded", function(){

    var button  = document.querySelector(".sign__button");
    var wrapper = document.querySelector(".sign__form-wrapper");
    button.addEventListener('click', function(){
        wrapper.classList.remove('hidden');
        button.classList.add('plain');
    })
});
