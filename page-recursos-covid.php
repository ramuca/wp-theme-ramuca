<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package ramuca
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.

		$resource_cats = get_terms([
				'taxonomy' => 'resource_cats',
				'hide_empty' => false,
		]);
		$districts = get_field_object('field_5e7df183f6f16')['choices'];
		?>

		<div class="resource-filters">
				Quiero ver los recursos sobre
				<select class="resource-filters__select" data-type="cat">
						<option value="all" selected>
								todos los temas
						</option>
						<?php foreach($resource_cats as $cat): ?>
						<option value="<?= $cat->term_id ?>">
								<?= $cat->name ?>
						</option>
						<?php endforeach; ?>
				</select>
				en
				<select class="resource-filters__select" data-type="hood">
						<option value="all" selected>
								todos los distritos
						</option>
						<?php foreach($districts as $district): ?>
						<option value="<?= sanitize_title($district) ?>">
								<?= $district ?>
						</option>
				<?php endforeach; ?>
				</select>
		</div>
		<?php
		$resources = get_posts([
				'post_type'      => 'resource',
				'posts_per_page' => '-1',
				'post_status'    => 'publish'
		]);
		foreach($resources as $resource)
		{
				$id = $resource->ID;
				$lnk = get_field('enlace', $id);
				$attachment = get_field('field_5e7768eee54df', $id);
				$district = get_field('field_5e7df183f6f16', $id);
				$cat = get_field('field_5e7dfc984d213', $id);
				?>

				<div class="resource" data-cat="<?= $cat[0]->term_id ?>" data-hood="<?= sanitize_title($district) ?>">
						<figure class="resource__image">
								<?php
								$img = get_the_post_thumbnail($id);
								if(isset($img) && trim($img)!=''): ?>
										<?= $img ?>
								<?php elseif($attachment): ?>
										<img class="placeholder" src="/wp-content/themes/ramuca/style/icons/file-text.svg" />
								<?php elseif( isset($lnk) && trim($lnk)!='' ): ?>
										<img class="placeholder" src="/wp-content/themes/ramuca/style/icons/external-link.svg" />
								<?php endif; ?>
						</figure>
						<div class="resource__data">
							  <p class="resource__categories">
									  <?php if(isset($cat)): ?>
										<span class="resource__categories-cat">
												<?= $cat[0]->name ?>
										</span>
										<?php endif; ?>
									  <?php if(isset($district) && trim($district)!='' ): ?>
										<span class="resource__categories-hood">
												<?= $district ?>
										</span>
										<?php endif; ?>
								</p>
								<h3 class="resource__data-name">
										<?= $resource->post_title ?>
								</h3>
								<p class="resource__data-description">
									  <?= $resource->post_content ?>
								</p>
								<ul class="resource__links">
									  <?php if(isset($lnk) && trim($lnk)!='' ): ?>
										<li class="resource__link">
												<a href="<?= $lnk ?>" target="_blank">
														<?= __('Ver recurso [página externa]', 'ramuca') ?>
												</a>
										</li>
										<?php endif ?>
										<?php if($attachment): ?>
										<li class="resource__link">
												<a href="<?= $attachment['url'] ?>" target="_blank">
														<?= __('Descargar archivo adjunto', 'ramuca') ?>
												</a>
										</li>
										<?php endif ?>
								</ul>
						</div>
				</div>
		<?php } ?>
		<div class="resources__empty hidden">
					No hay recursos disponibles con los criterios de filtrado que has seleccionado.
					Prueba con búsquedas más generales filtrando por 'todos los temas' o 'todos los distritos'
		</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
