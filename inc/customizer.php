<?php
/**
 * ramuca Theme Customizer
 *
 * @package ramuca
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function ramuca_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial( 'blogname', array(
			'selector'        => '.site-title a',
			'render_callback' => 'ramuca_customize_partial_blogname',
		) );
		$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
			'selector'        => '.site-description',
			'render_callback' => 'ramuca_customize_partial_blogdescription',
		) );
	}

		/**
		 *	Custom theme WP customizer
		 */
		$wp_customize->add_section('ramuca_settings' , [
	 			'title'      => __('Otros ajustes', 'ramuca' ),
	 			'priority'   => 30,
		]);
		$wp_customize->add_setting('od_form_slug__demand' , [
		    'transport' => 'refresh',
		]);
		$wp_customize->add_setting('od_form_slug__offer' , [
		    'transport' => 'refresh',
		]);
		$wp_customize->add_section('ramuca_settings' , [
	 			'title'      => __('Otros ajustes', 'ramuca' ),
	 			'priority'   => 30,
		]);
		$wp_customize->add_control(new WP_Customize_Control( $wp_customize, 'od_form_slug__demand', [
				'label'       => __( 'Slug del formulario de demandas', 'ramuca'),
				'description' => __('Pon aquí el slug del formulario para realizar demandas', 'ramuca'),
				'section'     => 'ramuca_settings',
				'settings'    => 'od_form_slug__demand',
		]));
		$wp_customize->add_control(new WP_Customize_Control( $wp_customize, 'od_form_slug__offer', [
				'label'       => __( 'Slug del formulario de ofertas', 'ramuca'),
				'description' => __('Pon aquí el slug del formulario para realizar ofertas', 'ramuca'),
				'section'     => 'ramuca_settings',
				'settings'    => 'od_form_slug__offer',
		]));
}
add_action( 'customize_register', 'ramuca_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function ramuca_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function ramuca_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function ramuca_customize_preview_js() {
	wp_enqueue_script( 'ramuca-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'ramuca_customize_preview_js' );
