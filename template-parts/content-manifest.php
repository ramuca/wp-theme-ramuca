<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package ramuca
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php
		if ( is_singular() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

		if ( 'post' === get_post_type() ) :
			?>
			<div class="entry-meta">
				<?php
				ramuca_posted_on();
				ramuca_posted_by();
				?>
			</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	<?php ramuca_post_thumbnail(); ?>

	<div class="entry-content">
		<?php
		the_content( sprintf(
			wp_kses(
				/* translators: %s: Name of current post. Only visible to screen readers */
				__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'ramuca' ),
				array(
					'span' => array(
						'class' => array(),
					),
				)
			),
			get_the_title()
		) );

		wp_link_pages( array(
			'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'ramuca' ),
			'after'  => '</div>',
		) );

    $supporters = explode("\n", get_field('firmantes'));
		?>
	</div><!-- .entry-content -->
  <div class="sign">
      <button class="sign__button">
            Firma el llamamiento
      </button>
      <div class="sign__form-wrapper hidden">
          <?= do_shortcode('[contact-form-7 id="371" title="Formulario para firmar manifiesto"]'); ?>
      </div>
  </div>
  <h2>
       Adhesiones <small>(<?= count($supporters) ?>)</small>
  </h2>
  <table class="manifest-supporters">
      <tr>
          <th></th>
          <th>Nombre de la persona u organización</th>
      </tr>
      <?php foreach($supporters as $i=>$supporter): $data = str_getcsv( $supporter ); ?>
      <tr class="manifest-supporters__item>">
            <td><?= $i+1 ?></td>
            <td><?= $data[0] ?></td>
      </tr>
      <?php endforeach ?>
  </table>
	<footer class="entry-footer">
		<?php ramuca_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
